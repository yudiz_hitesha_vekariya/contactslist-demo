import React, { useState, Fragment } from "react";
import "./App.css";
import data from "./mock-data.json";
import ReadOnlyRow from "./components/ReadOnlyRow";
import EditableRow from "./components/EditableRow";
import Pagination from "./components/Pagination";

const App = () => {
  const [contacts, setContacts] = useState(data);
  const [addFormData, setAddFormData] = useState({
    fullName: "",
    age: "",
    phoneNumber: "",
    email: "",
  });

  const [editFormData, setEditFormData] = useState({
    fullName: "",
    age: "",
    phoneNumber: "",
    email: "",
  });

  const [editContactId, setEditContactId] = useState(null);
  const [order, setOrder] = useState("ASC");
  const [searchTerm, setSearchTerm] = useState("");
  const [currPage, setCurrPage] = useState(1);
  const [postPerPage] = useState(4);

  const lastPost = currPage * postPerPage;
  const firstPost = lastPost - postPerPage;
  const currentPost = contacts.slice(firstPost, lastPost);

  const paginate = (pageNo) => {
    setCurrPage(pageNo);
  };

  const handleAddFormChange = (event) => {
    event.preventDefault();

    const fieldName = event.target.getAttribute("name");
    const fieldValue = event.target.value;

    const newFormData = { ...addFormData };
    newFormData[fieldName] = fieldValue;

    setAddFormData(newFormData);
  };

  const handleEditFormChange = (event) => {
    event.preventDefault();

    const fieldName = event.target.getAttribute("name");
    const fieldValue = event.target.value;

    const newFormData = { ...editFormData };
    newFormData[fieldName] = fieldValue;

    setEditFormData(newFormData);
  };

  const handleAddFormSubmit = (event) => {
    event.preventDefault();

    const newContact = {
      id: data.length + 1,
      fullName: addFormData.fullName,
      age: addFormData.age,
      phoneNumber: addFormData.phoneNumber,
      email: addFormData.email,
    };

    const newContacts = [...contacts, newContact];
    setContacts(newContacts);
  };

  const handleEditFormSubmit = (event) => {
    event.preventDefault();

    const editedContact = {
      id: editContactId,
      fullName: editFormData.fullName,
      age: editFormData.age,
      phoneNumber: editFormData.phoneNumber,
      email: editFormData.email,
    };

    const newContacts = [...contacts];

    const index = contacts.findIndex((contact) => contact.id === editContactId);

    newContacts[index] = editedContact;

    setContacts(newContacts);
    setEditContactId(null);
  };

  const handleEditClick = (event, contact) => {
    event.preventDefault();
    setEditContactId(contact.id);

    const formValues = {
      fullName: contact.fullName,
      age: contact.age,
      phoneNumber: contact.phoneNumber,
      email: contact.email,
    };

    setEditFormData(formValues);
  };

  const handleCancelClick = () => {
    setEditContactId(null);
  };

  const handleDeleteClick = (contactId) => {
    const newContacts = [...contacts];

    const index = contacts.findIndex((contact) => contact.id === contactId);

    newContacts.splice(index, 1);

    setContacts(newContacts);
  };
  const sorting = (col) => {
    if (order === "ASC") {
      const sorted = [...contacts].sort((a, b) =>
        a[col].toLowerCase() > b[col].toLowerCase() ? 1 : -1
      );

      setContacts(sorted);
      setOrder("DSC");
    }
    if (order === "DSC") {
      const sorted = [...contacts].sort((a, b) =>
        a[col].toLowerCase() < b[col].toLowerCase() ? 1 : -1
      );

      setContacts(sorted);
      setOrder("ASC");
    }
  };

  return (
    <div className="app-container">
      <h4 className="head">CONTACT DATALIST</h4>
      <div>
        <input
          type="search"
          // role="button"
          placeholder="search by name..."
          className="fo"
          onKeyPress={(e) => {
            console.log(e.target.value);
            setSearchTerm(e.target.value);
          }}
        ></input>
      </div>

      <h3 className="hey">
        please click to below any title for sorting(A & D) your data..
      </h3>
      <div>
        <form onSubmit={handleEditFormSubmit}>
          <table>
            <thead>
              <tr>
                <th className="sort" onClick={() => sorting("fullName")}>
                  Name
                </th>
                <th className="sort" onClick={() => sorting("age")}>
                  AGE
                </th>
                <th className="sort" onClick={() => sorting("phoneNumber")}>
                  Phone Number
                </th>
                <th className="sort" onClick={() => sorting("email")}>
                  Email
                </th>
                <th className="sort">Actions</th>
              </tr>
            </thead>
            <tbody>
              {searchTerm.trim().length > 0
                ? contacts
                    .filter((val) => val.fullName.toLowerCase().includes(searchTerm.toLowerCase()) )
                    .map((contact) => (
                      <Fragment>
                        {editContactId === contact.id ? (
                          <EditableRow
                            editFormData={editFormData}
                            handleEditFormChange={handleEditFormChange}
                            handleCancelClick={handleCancelClick}
                          />
                        ) : (
                          <ReadOnlyRow
                            contact={contact}
                            handleEditClick={handleEditClick}
                            handleDeleteClick={handleDeleteClick}
                          />
                        )}
                      </Fragment>
                    ))
                : currentPost?.map((contact) => (
                    <Fragment>
                      {editContactId === contact.id ? (
                        <EditableRow
                          editFormData={editFormData}
                          handleEditFormChange={handleEditFormChange}
                          handleCancelClick={handleCancelClick}
                        />
                      ) : (
                        <ReadOnlyRow
                          contact={contact}
                          handleEditClick={handleEditClick}
                          handleDeleteClick={handleDeleteClick}
                        />
                      )}
                    </Fragment>
                  ))}
            </tbody>
          </table>
        </form>
        {searchTerm.trim().length <= 0 && (
          <Pagination
            postPerPage={postPerPage}
            totalPosts={contacts.length}
            paginate={paginate}
          />
        )}

        <h2 className="hed">Add a Contact</h2>
        <form onSubmit={handleAddFormSubmit}>
          <input
            type="text"
            name="fullName"
            required="required"
            placeholder="Enter a name..."
            onChange={handleAddFormChange}
          />
          <input
            type="number"
            name="age"
            required="required"
            placeholder="Enter age..."
            onChange={handleAddFormChange}
          />
          <input
            type="number"
            name="phoneNumber"
            required="required"
            placeholder="Enter a phone number..."
            onChange={handleAddFormChange}
          />
          <input
            type="email"
            name="email"
            required="required"
            placeholder="Enter an email..."
            onChange={handleAddFormChange}
          />
          <button type="submit" className="btn">
            Add
          </button>
        </form>
      </div>
    </div>
  );
};

export default App;
