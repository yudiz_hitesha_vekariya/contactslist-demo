import React, { useRef } from "react";


import Searchbar from "./Searchbar";

function Searching() {
  

  const searchfor = useRef(null);

  function handleSearch(e) {
    e.preventDefault();
    
  }

  return (
    <form className="search">
      <Searchbar ref={searchfor} />
      <button className="btn" onClick={handleSearch}>
        Search now
      </button>
    </form>
  );
}

export default Searching;
