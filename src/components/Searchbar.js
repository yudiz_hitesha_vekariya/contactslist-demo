import React, { forwardRef } from "react";


function Searchbar(props, ref) {

  return (
    <input
      type="text"
      placeholder="Search by Name..."
      className="search"
      ref={ref}
    />
  );
}

export default forwardRef(Searchbar);
